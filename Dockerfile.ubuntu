FROM ubuntu:20.04

ENV DEBIAN_FRONTEND="noninteractive"

ARG	GOMPLATE_VERSION=3.7.0
ENV GOMPLATE_URL=https://github.com/hairyhenderson/gomplate/releases/download/v${GOMPLATE_VERSION}/gomplate_linux-amd64-slim
ARG CMAKE_VERSION=3.18.2
ENV CMAKE_URL=https://github.com/Kitware/CMake/releases/download/v${CMAKE_VERSION}/cmake-${CMAKE_VERSION}-Linux-x86_64.sh

RUN apt update -y && \
	apt install -y \
	build-essential \
	qt5-default \
	qtbase5-dev \
	nlohmann-json3-dev \
	curl \
	llvm-10 \
	git \
	gcovr \
	gcc \
	clang-10 \
	python3-pip \
	libssl-dev \
	libtinyxml2-dev \
	libssh-dev \
	libcurl4-openssl-dev \
	libtclap-dev && \
	ln -s /usr/bin/llvm-profdata-10 /usr/bin/llvm-profdata && \
	ln -s /usr/bin/llvm-cov-10 /usr/bin/llvm-cov && \
	ln -s /usr/bin/clang++-10 /usr/bin/clang++ && \
	ln -s /usr/bin/clang-10 /usr/bin/clang && \
	curl -L ${GOMPLATE_URL} > /usr/bin/gomplate && \
	chmod +x /usr/bin/gomplate && \
	pip3 install flask && \
	curl -L ${CMAKE_URL} > /tmp/install_cmake.sh && \
	bash /tmp/install_cmake.sh --prefix=/usr --skip-license && \
	git clone https://github.com/catchorg/Catch2.git /opt/catch2 && \
	cd /opt/catch2 && mkdir build && cd build && \
	cmake -D BUILD_TESTING=OFF .. && \
	cmake --build . --target install && \
	git clone https://github.com/gabime/spdlog.git /opt/spdlog && \
	mkdir /opt/spdlog/build && cd /opt/spdlog/build && \
	cmake .. && make -j && make install && \
	cd / && \
	rm -rf /opt/spdlog /opt/catch2
